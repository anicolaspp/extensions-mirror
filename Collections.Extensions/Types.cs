﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using Nikos.Extensions.Collections;
using System.IO;

namespace Nikos.Extensions.Types
{

    public static class Types
    {
        /// <summary>
        /// Get the base class type of this class and implemented interfaces
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IEnumerable<Type> BaseTypes<T>(this T source)
        {
            var types = new Queue<Type>();
            types.Enqueue(source.GetType());

            while (types.Count > 0)
            {
                var current = types.Dequeue();
                yield return current;

                types.Enqueue(current.BaseType);
                types.AddRange(current.GetInterfaces());
            }
        }

        ///<summary>
        /// Compare the item with a IComparable by func result
        ///</summary>
        ///<param name="source">Item</param>
        ///<param name="obj">A IComparable</param>
        ///<param name="func">Function to convert type of T to ICompareble</param>
        ///<typeparam name="T">The type to compare with the ICompareble</typeparam>
        ///<returns>The value of comparison</returns>
        public static int CompareTo<T>(this T source, IComparable obj, Func<T, IComparable> func)
        {
            var t = func(source);
            return t.CompareTo(obj);
        }

        ///<summary>
        /// Compare two diferent objects by some inside property
        ///</summary>
        ///<param name="source">First object to compare</param>
        ///<param name="obj">Second object to compare</param>
        ///<param name="func1">Function to convert type of T to ICompareble</param>
        ///<param name="func2">Function to convert type of K to ICompareble</param>
        ///<typeparam name="T">The type to compare with the ICompareble</typeparam>
        ///<typeparam name="K">The type to compare with the ICompareble</typeparam>
        ///<returns>The value of comparison</returns>
        public static int CompareTo<T, K>(this T source, K obj, Func<T, IComparable> func1, Func<K, IComparable> func2)
        {
            return func1(source).CompareTo(func2(obj));
        }

		public static string ToXml<T> (this T obj)
		{
            string result = string.Empty;
			
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
			using (System.IO.MemoryStream ms = new System.IO.MemoryStream()) 
			{
				serializer.Serialize(ms, obj);
                using (System.IO.StreamReader reader = new System.IO.StreamReader(ms))
                {
                    result = reader.ReadToEnd();
                }
			}

			return result;
		}

		public static T GetObjectFromXml<T> (this string xmlRepresentation) where T : class 
		{
            T result;

			using (System.IO.MemoryStream ms = new System.IO.MemoryStream()) 
			{
			    using (StreamWriter writer = new StreamWriter(ms))
			    {
                    writer.Write(xmlRepresentation);
			    }

                result = ms.GetObjectFromXmlStream<T>();
			}

            return result;
		}

		public static T GetObjectFromXmlStream<T> (this Stream stream) where T : class
		{
            if (stream == null)
                throw new ArgumentNullException("stream");

			System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
			return serializer.Deserialize(stream) as T;
		}

		public static void SaveXmlRepresentationToFile<T>(this T obj, string filePath)
		{
			System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
			using (FileStream stream = new System.IO.FileStream(filePath, System.IO.FileMode.Create)) 
			{
				serializer.Serialize(stream, obj);
			}
		}

        public static string ToJson<T>(this T obj)
        {
            string result = string.Empty;

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, obj);
                using (StreamReader reader = new StreamReader(ms))
                {
                    result = reader.ReadToEnd();
                }
            }
            return result;
        }

        public static T GetObjectFromJson<T>(this string jsonRepresentation) where T : class 
        {
            T result;

            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(ms))
                {
                    writer.Write(jsonRepresentation);
                }

                result = ms.GetObjectFromJsonStream<T>();
            }

            return result;
        }

        public static T GetObjectFromJsonStream<T>(this Stream stream) where T : class
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            return serializer.ReadObject(stream) as T;
        }

        public static void SaveJsonRepresentationToFile<T>(this T obj, string filePath) where T: class 
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            using (FileStream stream = new FileStream(filePath, FileMode.Create))
            {
                serializer.WriteObject(stream, obj);
            }
        }
    }
}